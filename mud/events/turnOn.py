from .event import Event3
from .event import Event2

class TurnOnWithEvent(Event3):
    NAME = "turn-on"

    def perform(self):
        if not self.object.has_prop("turnOn-with"):
            self.fail()
            return self.inform("turn-on-with.failed")
        if not self.object2.has_prop("turn-on"):
            self.fail()
            return self.inform("turn-on-with.failed")
        self.inform("turn-on-with")


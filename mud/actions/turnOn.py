from .action import Action3
from mud.events import TurnOnWithEvent


class TurnOnWithAction(Action3):
    EVENT = TurnOnWithEvent
    ACTION = "turn-on-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"